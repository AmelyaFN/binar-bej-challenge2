package Service;

import Controller.MenuController;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Writing extends MenuController {

    public static void WriteFrekuensi (){
        try {
            File file = new File(FILEPATHWRITE1);
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);
            bwr.write (Counting.Frekuensi());
            bwr.newLine();
            bwr.flush();
            bwr.close();
        }
        catch (IOException e){
            System.out.println("Gagal menuliskan file.");
            e.printStackTrace();
        }
    }

    public static void WriteMeanMedianModus (){
        try {
            File file = new File(FILEPATHWRITE2);
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);
            bwr.write(("""
                     Hasil Pengolahan Nilai
                --------------------------------
                """ +"\n"+
                    " Mean : " + Counting.Mean()+"\n" +
                    "Median: " + Counting.Median()+"\n" +
                    "Modus : " + Counting.Modus()));
            bwr.newLine();
            bwr.flush();
            bwr.close();
        }
        catch (IOException e){
            System.out.println("Gagal menuliskan file.");
            e.printStackTrace();
        }
    }

}
