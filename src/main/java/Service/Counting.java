package Service;

import Controller.MenuController;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Counting extends MenuController {

    //Frekuensi
    static String Frekuensi(){
        List<Integer> listNilai = new ArrayList<Integer>();
        try{
            File file = new File(FILEPATHREAD);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            String [] tempArr;
            while ((line = br.readLine()) != null){
                tempArr = line.split(";");
                for (int i=1; i<tempArr.length; i++){
                    listNilai.add(Integer.parseInt(tempArr[i]));
                }
            }
            br.close();
        } catch (Exception e) {
            NotFound();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("""
                     Nilai      |     Frekuensi     
                ----------------|-------------------
                """);
        //sb.append("\n");
        int counter = 0;
        //Untuk kurang dari 6
        for (int i = 0; i < listNilai.size(); i++){
            if (listNilai.get(i) < 6){
                counter ++;
            }
        }
        sb.append("Angka < 6\t| "+ counter+"\n");
        //Untuk nilai 6 keatas
        for (int i = 6; i<=10; i++){
            counter =0;
            for (int j = 0; j < listNilai.size(); j++){
                if (listNilai.get(j)==i){
                    counter ++;
                }
            }
            sb.append(i +"\t\t| "+ counter+"\n");
        }
        return sb.toString();
    }

    // Mean
    static String Mean (){
        List<Integer> listNilai = new ArrayList<Integer>();
        try{
            File file = new File(FILEPATHREAD);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            String [] tempArr;
            while ((line = br.readLine()) != null){
                tempArr = line.split(";");
                for (int i=1; i<tempArr.length; i++){
                    listNilai.add(Integer.parseInt(tempArr[i]));
                }
            }
            br.close();
        } catch (Exception e) {
            NotFound();
        }
        float total = 0;
        for (int t : listNilai){
            total = total + t;
        }
        float hasil = total/ listNilai.size();
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(hasil);
    }

    //Median
    static float Median(){
        List<Integer> listNilai = new ArrayList<Integer>();
        try{
            File file = new File(FILEPATHREAD);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            String [] tempArr;
            while ((line = br.readLine()) != null){
                tempArr = line.split(";");
                for (int i=1; i<tempArr.length; i++){
                    listNilai.add(Integer.parseInt(tempArr[i]));
                }
            }
            br.close();
        } catch (Exception e) {
            NotFound();
        }
        for (int i=0; i<listNilai.size(); i++){
            for (int j=0; j< listNilai.size()-1;j++){
                if (listNilai.get(j) > listNilai.get(j+1)){
                    int temp = listNilai.get(j);
                    listNilai.set(j, listNilai.get(j+1));
                    listNilai.set(j+1, temp);
                }
            }
        }
        float hasil = 0;
        int indexMedian = listNilai.size()/2;
        if (listNilai.size()%2==0){
            hasil = listNilai.get(((indexMedian + (indexMedian + 1)) / 2));
        } else {
            hasil = listNilai.get(indexMedian+1);
        }
        return hasil;
    }

    //Modus
    static int Modus(){
        List<Integer> listNilai = new ArrayList<Integer>();
        try{
            File file = new File(FILEPATHREAD);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            String [] tempArr;
            while ((line = br.readLine()) != null){
                tempArr = line.split(";");
                for (int i=1; i<tempArr.length; i++){
                    listNilai.add(Integer.parseInt(tempArr[i]));
                }
            }
            br.close();
        } catch (Exception e) {
            NotFound();
        }
        for (int i=0;i< listNilai.size();i++) {
            for (int j=0;j< listNilai.size()-1;j++){
                if (listNilai.get(j) > listNilai.get(j + 1)){
                    int temp = listNilai.get(j);
                    listNilai.set(j, listNilai.get(j + 1));
                    listNilai.set(j + 1, temp);
                }
            }
        }
        int counter = 0;
        int countertmp = 0;
        int hasil = 0;
        for (int i=0; i<=10; i++){
            for (int j=0; j< listNilai.size();j++){
                if (listNilai.get(j)==i){
                    counter++;
                }
                if (counter > countertmp){
                    hasil = i;
                    countertmp = counter;
                }
            }
            counter = 0;
        }
        return hasil;
    }

}
