package Service;


import Controller.MenuController;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class ReadFile extends MenuController {

    static void readingFile() {
        List<Integer> listNilai = new ArrayList<Integer>();
        try {
            File file = new File(FILEPATHREAD);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            String[] tempArr;
            while ((line = br.readLine()) != null) {
                tempArr = line.split(";");
                for (String tempStr : tempArr) {
                    System.out.print(tempStr + " ");
                }
                System.out.println();
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        System.out.println("\n Tekan sembarang untuk kembali ke menu utama");
//        promptInput();
        
    }



//    public static void readingFile (String filePath, String delimiter) {
//        try{
//            File file = new File(filePath);
//            FileReader fr = new FileReader(file);
//            BufferedReader br = new BufferedReader(fr);
//            String line = "";
//            String[] tempArr;
//            //list
//            List<String> kls = new ArrayList<>();
//            while ((line = br.readLine()) != null) {
//                tempArr = line.split(delimiter);
//                System.out.println("Nama " + tempArr[0] + " " + tempArr[1] + " sebagai " + tempArr[2]);
//            }
//            br.close();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


}
