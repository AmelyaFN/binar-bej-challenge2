package Controller;

import Service.Writing;

import java.util.Scanner;

public abstract class MenuController {

    public static String FILEPATHREAD = ("src/main/resources/data_sekolah.csv");
    public static String FILEPATHWRITE1 = ("src/main/resources/data_sekolah_frekuensi.txt");
    public static String FILEPATHWRITE2 = ("src/main/resources/data_sekolah_mean_median_modus.txt");
    public static final Scanner scanner = new Scanner(System.in);

    private static byte promptInput() {
        System.out.print("----> ");
        return scanner.nextByte();
    }

    static void notValidInput() {
        System.out.println("""
        -----------------------------------------------------
            Pilihan tidak dikenali ! Silahkan pilih lagi.
        -----------------------------------------------------
        """);
        MenuUtama();
    }

    static void closeApp() {
        System.out.println("Aplikasi ditutup !");
        System.exit(0);
    }

    public static void MenuUtama() {
        System.out.println("""
                -------------------------------------
                Aplikasi Pengolah Nilai Siswa
                -------------------------------------
                Letakkan file data_sekolah.csv dalam direktori
                
                Pilih Menu :\s
                1. Generate file txt untuk menampilkan data Frekuensi Siswa
                2. Generate file txt untuk menampilkan data Mean, Median, dan Modus Siswa
                0. Tutup aplikasi""");
        switch (promptInput()) {
            case 0 :
                closeApp();
            case 1 :
                Writing.WriteFrekuensi();
                MenuDone();
                break;
            case 2 :
                Writing.WriteMeanMedianModus();
                MenuDone();
                break;
            default : notValidInput();
        }
    }

    static void MenuDone() {
        System.out.println("""
                -------------------------------------
                Aplikasi Pengolah Nilai Siswa
                -------------------------------------
                File telah digenerate ! Silahkan cek.\s
                1. Kembali ke Menu Utama
                0. Tutup aplikasi""");
        switch (promptInput()) {
            case 0 -> closeApp();
            case 1 -> MenuUtama();
            default -> notValidInput();
        }
    }

    public static void NotFound() {
        System.out.println("""
                -------------------------------------
                Aplikasi Pengolah Nilai Siswa
                -------------------------------------
                File tidak ditemukan ! Silahkan cek.\s
                1. Kembali ke Menu Utama
                0. Tutup aplikasi""");
        switch (promptInput()) {
            case 0 -> closeApp();
            case 1 -> MenuUtama();
            default -> notValidInput();
        }
    }

}
